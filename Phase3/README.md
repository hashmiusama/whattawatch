## CSE 515 - Project Phase 3
## Group number 2

## System Requirements
Python 2.7.9
Python libraries
	-scikit-learn
	-numpy
	-sparsesvd
	-scikit-tensor
	-numpy+mkl
	-pandas
Operating System: Mac/Windows/Linux

## Instructions to run phase2 project

//First iteration will take approximately 3 mins to complete
//Successive iterations will be faster
#Task 1a 
Input Command: python task1a_pca.py
Input Command: python task1a_svd.py

Console will ask for user id during runtime

//First iteration will take approximately 3 mins to complete
//Successive iterations will be faster
Input Command: python task5a.py
Console will ask for labels and list of movies under it

# Task 1b and 2b
Input: python task1b.py tag tf <user-id>
it will sak for movies you do not like. Enter space seperated movie ids and press enter. e.g. - 2 4 5
To quit the program press CTRL+C

# Task 1c&2c
Input: python task1c2cFor1e.py
Console will ask for userid
After the list of movies is displayted console will be 1
Then user will be prompted to enter either 1 or 0 for each movie based on relevance.
Update list will be shown followed by a prompt asking if the user wants to continue or not.
Press y to continue and n to stop.

# Task 4
python task4.py
it will ask for feedback. Enter movie ids you liked seperated by space
To quit the program press CTRL+C

# Task 5b
python task5b.py
It will ask for training data
input space seperated movie ids first, press enter
now enter space seperated labels
It will ask for movie to predict. Enter movie id