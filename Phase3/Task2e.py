import t1a4e as apca
import task1d_for_1e as ppr
import task1c2c as cp
import task1bfor1e as lda
import dataframes as df
userID = cp.userid

def task2e():
    # movieid, similarity
    pca_df = apca.get_similar_movie(apca.__get_movies_watch_by_user__(int(userID)), int(userID))
    max_sim = max(pca_df['similarity'])
    min_sim = min(pca_df['similarity'])
    pca_df['normsim'] = (max_sim - pca_df['similarity'])/(max_sim - min_sim + 1)
    # movieid, pagerank
    ppr_df = ppr.get_output(userID)
    max_pr = max(ppr_df['pagerank'])
    min_pr = min(ppr_df['pagerank'])
    ppr_df['normpr'] = (max_pr - ppr_df['pagerank'])/(max_pr-min_pr+1)

    # movie, freq -> name here not id
    cp_df = cp.task1cFunc(userID)
    max_freq = max(cp_df['freq'])
    min_freq = min(cp_df['freq'])
    cp_df['normfreq'] = (max_freq - cp_df['freq'])/(max_freq - min_freq + 1)
    # movie, dist -> id
    lda_df = lda.get_lda_recommendations(userID, 'tf')
    max_dist = max(lda_df['dist'])
    min_dist = min(lda_df['dist'])
    cp_df['normdist'] = (max_dist - lda_df['dist'])/(max_dist - min_dist + 1)

    df.ML_MOVIES.join(cp_df, on='movie')

