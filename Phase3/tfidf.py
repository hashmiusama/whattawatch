import sys
import numpy as np
import pandas as pd
from collections import OrderedDict
from itertools import izip, repeat
import datetime as dt

# Read mltags CSV file
mltags_df = pd.read_csv("Data/mltags.csv", keep_default_na=False, na_values=[""])

# Convert timestamp to seconds (find epoch)
mltags_df['timestamp'] = pd.to_datetime(mltags_df['timestamp'])
mltags_df['timestamp'] = (mltags_df['timestamp'] - dt.datetime(1970, 1, 1)).dt.total_seconds()

# Normalize the timestamp to a range between 1 and 2
mltags_df['timestamp'] = ((mltags_df['timestamp'] - mltags_df['timestamp'].min()) / (
mltags_df['timestamp'].max() - mltags_df['timestamp'].min())) + 1

# Read movie-actor CSV file
# movie_actor_df = pd.read_csv("movie-actor.csv", keep_default_na=False, na_values=[""])
# movies = pd.read_csv("mlmovies.csv")


# Number of movies for a given tagid
def df_for_one_tag(tagid):
    # Consider only those rows where there is given tagid
    df_for_one_tagid = mltags_df.loc[mltags_df['tagid'] == tagid]

    # Drop all duplicate movieids
    df_for_one_tagid = df_for_one_tagid.drop_duplicates('movieid')

    # number of movies for given tagid
    movie_count_for_tag = len(df_for_one_tagid)

    # Return movie_count
    return movie_count_for_tag


def function(movieid):
    # declare merged as global so that df_for_one_tag() can access it
    # global merged

    # Merge the two csv files
    # merged = pd.merge(mltags_df, movies, on='movieid')
    # del mltags_df['userid']

    # Consider only those rows where there is given mltags_dfid
    df_for_one_movieid = mltags_df.loc[mltags_df['movieid'] == movieid]

    # Find number of tagids for movie
    tag_count_for_movie = df_for_one_movieid.groupby('movieid').count().reset_index()
    tag_count_for_movie['tag_count_for_movie'] = tag_count_for_movie['movieid']
    # del tag_count_for_movie['movieid']
    del tag_count_for_movie['timestamp']
    del tag_count_for_movie['tagid']

    # Merge the above two dataframes
    movie_tag_df = pd.merge(df_for_one_movieid, tag_count_for_movie, on='movieid')

    # Find mean of timestamps for same tagid
    timestamp_mean_df = movie_tag_df.groupby('tagid').mean().reset_index()
    timestamp_mean_df['timestamp_mean'] = timestamp_mean_df['timestamp']
    del timestamp_mean_df['timestamp']

    # Find total number of tags associated with actor
    num_of_tags_for_movie = timestamp_mean_df['tag_count_for_movie'].sum()

    if num_of_tags_for_movie == 0:
        return "Nothing found"

    # Calculate tf
    timestamp_mean_df['tf'] = timestamp_mean_df['tag_count_for_movie'] / num_of_tags_for_movie
    # timestamp_mean_df['tf'] = timestamp_mean_df['tf'] / timestamp_mean_df['actor_movie_rank']
    timestamp_mean_df['tf'] = timestamp_mean_df['tf'] * timestamp_mean_df['timestamp_mean']

    # tagid to list
    tagid_to_list = timestamp_mean_df['tagid'].tolist()

    # Count the total num of movies in csv file
    movieid = mltags_df['movieid'].values
    movieid_nodup = list(OrderedDict(izip(movieid, repeat(None))))
    num_of_movies = len(movieid_nodup)

    count = []

    # iterate through all tagids for given movieid and append it to a list
    for i in range(len(timestamp_mean_df)):
        count.append(df_for_one_tag(tagid_to_list[i]))

    timestamp_mean_df['num_of_movie_with_tag'] = count

    # Calculate idf
    timestamp_mean_df['idf'] = num_of_movies / timestamp_mean_df['num_of_movie_with_tag']
    timestamp_mean_df['idf'] = np.log(timestamp_mean_df['idf'])

    # Calculate tfidf
    timestamp_mean_df['tfidf'] = timestamp_mean_df['tf'] * timestamp_mean_df['idf']

    tagTFIDF = dict(zip(timestamp_mean_df.tagid, timestamp_mean_df.tfidf))
    return tagTFIDF
